# Politique de contribution aux logiciels libres

## Objectifs:

 * Définir un cadre juridique et technique pemettant et encourageant la contribution des agents 
 * Accompagner et former les agents sur les méthodes de contribution
 * Faciliter les interactions entre les administrations et les communautés des Logiciels Libres qu'elles utilisent
 * Faciliter l'intégration des développements réalisés par des prestataires dans les projets Libres auxquels ils sont liés
 * Définir des règles et des bonnes pratiques et favoriser leur diffusion au sein des administrations. 

## Espace de discussion:
 
  * Le sujet principal sur le formum d'Étalab: https://forum.etalab.gouv.fr/t/politique-de-contribution-aux-logiciels-libres/86
  * Afin de profiter des débats qui ont déjà eu lieu sur les questions concernées par le sujet, il sera pertinent de liers les fils ouverts sur le forum (comme https://forum.etalab.gouv.fr/t/quelles-licences-pour-les-logiciels-libres-du-secteur-public/292/15 )


## Documents 

 * La [politique de contribution](oss-policy.md)
 * Un modèle de [contributor's license agreement](CLA.md) inspiré du logiciel [Pilgrim](http://pilgrim.univ-nantes.fr/wp-content/uploads/2015/11/PILGRIM-GPL-CLA-2015-04-21.pdf)
On évaluera également les enseignments pouvant être tirés du projet [Harmony Agreements](http://www.harmonyagreements.org/) 
 
 L'ensemble des documents sont disponibles sous licence [CC-BY-3.0](LICENSE)

## Credits

S'inspire des documents suivants:
 * USA 18F: https://github.com/18F/open-source-policy/blob/master/CONTRIBUTING.md
 * UK GDS : http://gds-operations.github.io/guidelines/
 * Canada British Columbia : https://github.com/bcgov/BC-Policy-Framework-For-GitHub