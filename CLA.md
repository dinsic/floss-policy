 * Contributeur : 
 * Nom :
 * Prénom : 
 * Société : 
 * Adresse complète du contributeur :
 * Pays : 
 * Téléphone : 
 * Courriel : 

# OBJET : 

Afin d’intégrer sa Contribution au Projet, le Contributeur accepte les conditions contractuelles suivantes. 

# DEFINITIONS : 
 
 * __Contributeur__ : le titulaire des droits de propriété intellectuelle, ou la personne (qu’elle soit physique ou morale) dûment autorisée par le titulaire des droits d’auteur à conclure le présent contrat. 
 * __Contribution__ : désigne l'ensemble des modifications, corrections, traductions, adaptations et/ou nouvelles fonctionnalités intégrées par le Contributeur, et protégées, ou susceptibles d’être protégés par un droit de propriété intellectuel. Par « intégré », on entend toute contribution communiquée par quelque moyen que ce soit à **#########**. Ne seront pas considérés comme une Contribution les parties du code communiquées à **#########** qui sont clairement, précisément et expressément désignées comme exclues du présent contrat par le Contributeur au moment de la communication. 

# LICENCE DE DROITS D’AUTEUR :

Conformément à l’objet du présent contrat, le Contributeur concède à **#########** une licence irrévocable, mondiale, non exclusive et gratuite, de sa Contribution pour leur durée légale de protection. Plus particulièrement, ces droits comprennent les droits d’utilisation, de reproduction, de représentation, d’adaptation et de mise sur le marché étant précisé que :
 
 - le droit d’utilisation comporte notamment le droit d’utiliser la Contribution pour tous usages, à des fins de recherche et développement ou d’exploitation, pour les besoins propres de **#########** ou au profit de tiers,
 - le droit de reproduction comporte notamment le droit de procéder à toute reproduction nécessaire aux actes de chargement, affichage sur écran, exécution, transmission, stockage, le droit de reproduire ou de faire reproduire la Contribution, par tous moyens, sous toutes formes et sur tous supports, notamment informatiques (ce qui inclut tous les moyens de stockages informatiques existants, notamment, tout disque dur, serveur interne ou externe etc.) virtuels ou non, papier (documentation technique) ou sur tout autre support, en un nombre d’exemplaires illimité, par tout moyen présent et à venir, ou sur tous réseaux analogiques ou numériques, privatifs ou ouverts au public (Internet, Intranet), nationaux et/ou internationaux,
 - le droit de représentation comporte notamment le droit de diffuser la Contribution auprès de toute personne, par quelque moyen que ce soit (par téléchargement, partage, mise à disposition sur tous types de réseaux, libre accès sur Internet etc.), 
 - le droit d’adaptation comporte le droit d’adapter en tout ou partie, d’arranger, de corriger les erreurs, de traduire en tout langage, ou de modifier de toute autre façon la Contribution notamment par suppression, ajout, intégration totale ou partielle dans un autre logiciel, et de reproduire, utiliser et mettre sur le marché comme défini au présent article les logiciels en résultant, 
 - le droit de mise sur le marché comporte notamment le droit de commercialiser la Contribution, de la distribuer, louer, à titre gratuit ou onéreux, prêter, ou d’assurer toute prestation de service utilisant directement ou indirectement la Contribution, et/ou d’accorder à des tiers, tant en France qu’à l’étranger par voie de cession ou de concession de licence, exclusive ou simple, transférable ou non transférable, à titre gratuit ou onéreux, tout ou partie des droits d’utilisation, de reproduction, de représentation, d’adaptation et/ou de mise sur le marché de la Contribution. 

# LICENCE DE BREVETS :

Conformément à l’objet du présent contrat, le Contributeur concède à **#########**, et le cas échéant ses éventuels mandataires, une licence irrévocable, mondiale, non exclusive et gratuite de tout brevet présent ou à venir relatif à sa Contribution pour la durée de protection desdits brevets aux fins d’offrir, mettre dans le commerce, utiliser, importer, ou détenir la Contribution. Cette licence est limitée aux revendications desdits brevets susceptibles d’entraver la réalisation du Projet. 

# GARANTIE : 
Le Contributeur garantit que chaque Contribution est originale et qu’elle lui appartient. Il garantit également que l’ensemble des Contributions ne contiennent aucune limite d’utilisation, et ce notamment au regard des autres éventuels droits d’auteur, de marque, et ou de brevets, de même il garantit qu’il ne viole aucune obligation de confidentialité. Lorsque les droits de propriété intellectuelle portant sur le Contribution appartiennent l’employeur du Contributeur, le Contributeur garantit qu’il a été autorisé à communiquer sa Contribution et à la placer sous le régime juridique découlant du présent contrat, ou, que son employeur s’est lui-même soumis à la signature d’un contrat identique au profit de **#########**. La Contribution est délivrée en l’état, sans aide à l’utilisation, et sans garantie relative à son adéquation à une quelconque utilisation technique et/ou commerciale. Le Contributeur s’engage à notifier promptement à **#########** tout fait ou circonstance dont il a connaissance susceptible de compromettre ou limiter les droits concédés au terme des présentes.

# LOI APPLICABLE - LITIGE 
Ce contrat est régi par la loi française et relève de la seule compétence des tribunaux français.

 * Fait à :
 * Le:
 * Par :
 * Signature:
