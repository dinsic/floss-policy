# Politique de contribution aux logiciels libres

Fil de discussion sur la politique logiciels libres : https://forum.etalab.gouv.fr/t/politique-de-contribution-aux-logiciels-libres/86

__Historique et versions__

| Version | Commentaire    | Date       |
| --------|----------------|------------|
| 0.1     | Initialisation | 25/04/2016 |

[![CC-BY][CC-BY Logo]][CC-BY link] Document publié en [**CC-BY**][CC-BY link]


1.Généralités
==============

1.1. Cadre et portée
---------------------

Concerne tous les ministères et opérateurs, pour les développements spécifiques internes ou pour ceux réalisés dans le cadre de prestation.
Les collectivités sont libres de s'y référer

Ne concerne que les contributions aux codes sources des logiciels, ainsi que les tickets d'anomalies. RAJOUTER LA PARTIE DOCUMENTATION AU PERIMETRE ? DANS CE CAS BESOIN DE PRECISER LA LICENCE ET LA LANGUE. Tout ce qui est contenu ou documentation sera traité dans le cadre d'une autre politique (open data / open content)
Préciser qu'il faut intégrer les outils de build ? Déploiement ?

Cette politique s'inspire de la politique open-source dévelopée aux Etats-Unis par 18F.

Objectif d'être un document auquel tous les développeurs au service de l'Etat pourront se référer. A pour objectif d'être mis à jour régulièrement et maintenu, n'hésitez pas à soumettre des issues ou faire des pull requests pour l'améliorer et prendre en compte les différentes situations.

Est une partie à la réponse du besoin de contribution de l'administration: https://forum.etalab.gouv.fr/t/contribution-de-l-administration-aux-logiciels-libres/265

1.2. Stratégie globale
-----------------------

Rappel de la [circulaire Ayrault][Circulaire Ayrault]

Faciliter l'ouverture et la réutilisation des développements réalisés ou financés par l'Etat, pour viser la transparence et l'efficacité de l'utilisation des deniers publics.

L'Etat n'a pas vocation a être un éditeur logiciel. Dans le cadre de nouveau projet, l'objectif est de trouver une organisation, association ou fondation qui assurerait la gouvernance du projet.
Ces dernières ont la possibilité d'être francaise ou non.
Exemple comme Apache fondation, etc.

Trois niveaux de contributions sont à retenir:
 1. Contribution à un projet existant
 2. Rôle de commiter dans un projet non initié par l'Etat
 3. Projet initié par l'Etat

Les niveaux 1. et 2. sont traités dans la partie 2, tandis que le niveau 3 est traité dans la partie 3.

Stipuler qu'il ne peut y avoir de sanctions si cette politique est respectée ? (rajouter des checklistes?)

1.3. Déclinaison en politique de contributions ministérielles
--------------------------------------------------------

Doivent reprendre ces principes mais peuvent etre déclinées de manière plus précise par ministère (définition des repository, compte d'organisation, etc.)

1.4. Utilisation des comptes professionnels et personnels
----------------------------------------------------

Mise en valeur des contributions individuelles

Distinguer les deux types de contribution pro / perso.

 * pour les agents

  OK et même nécessaire d'utiliser son mail pro en .gouv.fr dans le cadre de contributions rémunérées sur le temps de travail

 * pour les prestataires

  Préciser comment on fait pour les prestataires : quelle clauses ?
  Mail du prestataire professionnel même si payé par l'Etat? (ou utiliser au contraire un mail externe pour un presta en -ext ou @prestataires.gouv.fr pour ceux à qui on donne le droit de commit)

Préconisation d'avoir un seul compte par plateforme (github/framagit) pour les contributions pro et perso. 
Les contributions personnelles doivent être faites avec le mail personnel, et les contributions professsionnelles avec le compte professionnel.
Voir les recommandations en cas de départ ou changement d'entité professionnelle:
https://help.github.com/articles/best-practices-for-leaving-your-company/

Exemple d'utilisation avec le code github

 * Avoir deux repository
  * work/src/mon-projet-pro
  * work/src/mon-projet-perso

 * Dans mon-projet-pro

```
git config user.email <email@pro.gouv.fr>
 ```
 
 * Dans mon projet-perso

```
git config user.email <email@perso.fr>
```

Vous pouvez vérifier la bonne prise en compte du mail par la commande:
```
git config --get user.email
```

Bien qu'il soit possible d'aoir deux comptes séparés, ce n'est pas la solution recommandée, les plateformes publiques laissant généralement la possibilité de gérer des comptes d'organisation. Si vous souhaitez fusionner vos comptes sur une plateforme, vous trouverez une procédure : https://help.github.com/articles/merging-multiple-user-accounts/

Détailler la possibilité de contributions anonymes ou sous un pseudonyme? 

1.5. Formation et montée en compétences des agents
---------------------------------------------------

Objectif d'avoir une capacité à faire

Avoir un outil de versioning de code privé avant de commiter publiquement pour entrainer les agents

Mise en oeuvre de peer review au sein des ministère

Garder les roles de commiters

1.6. Cadrage des contributions financées par l'Etat
---------------------------------------------------

Clarification des attentes de l'Etat en matière de logiciels libres pour les prestataires et indépendants.

1.7. Assistance et support
---------------------------

 * DINSIC tient une adresse email et une FAQ disponible
 * Rappel des marchés de support logiciels libres
 * Rappel des marchés de formation

2. Contribution à des projets existants
=======================================

2.1 Contributions encouragées
----------------------------

Utilisation de comptes nominatifs professionnels. Possibilité d'utiliser des comptes "organisation"s si la contribution de code passe par un commiter du projet.
Un rôle de comiter se fait au niveau nominatif.

Pour contribuer regarder quelles sont les modalités de contribution proposées par le projet

Clauses de contirbutions pour le cadre d'un prestataires

Reprise des exemples de contribution des marchés de supports

Soumission de bug et de patchs

 * Via marché de support
 * Directement auprès du projet si public / anonyme / pas de besoin de SLA

Aujourd'hui les marchés de support ne contienne pas d'engagement à ce qu'un patch soit commité dans un projet en particulier.

2.2. Licences
-------------

Nécessité de respecter la licence du projet, du moment que celle-ci est OSI compatible. Sinon demander quelle action prendre au support DINSIC.

Attention du code sans licence est par défaut protégé.


2.3. Procédure de validation
-----------------------------

Pas de validation obligatoire : c'est le rôle des commiters des projets

Respect des procédures de validation interne aux ministères si besoin.

2.4. Suivi et enregistrement
----------------------------

Pas nécessaire de faire un suivi et un enregistrement de toutes les contributions, même si ca reste possible et peut être valorisant.

Toutefois, en cas de récupération d'un rôle de commiter par une personne de l'Etat dans le cadre de son activité professionnelle : besoin de recensement voir la section 3.6.

3. Nouveaux projets contribués
=====================================

Un fork correspond à un nouveau projet contribué / Un module indépendant aussi (surtout si le choix de la licence se pose)

3.1. Choix de la licence
-----------------------

Discussion sur le choix de la licence: https://forum.etalab.gouv.fr/t/quelles-licences-pour-les-logiciels-libres-du-secteur-public/292/18

Outil générique d'aide au choix de la licence : http://choosealicense.com/

Description synthétiques des licences: https://tldrlegal.com/

 * Proposer un arbre de décision sur les licences
 * Choisir une licence c'est choisir une communauté de développeur
 * Possibilité de CC0 (problème avec lse prestataires)
 * Afficher la licence MIT
 * Afficher la licence CeCill
 * Afficher la licence EUPL

Licence différente en cas de production de code par un sous-traitant (pas de possibilité de faire du CC0 ?)

3.2. Repository
--------------

Lien vers le fil de discussion : https://forum.etalab.gouv.fr/t/forges-de-logiciels/130

 * Mettre un lien vers les critères de la FSF et préférer le git de framasoft
 * Dire qu'il n'y a explicitement pas de règles sur le sujet, l'important étant la disponibilité du code source, l'accès à l'historique des modifications et la possibilité d'y contribuer
 * Expliciter que github est OK
 * Faut-il obliger à l'utilisation d'un DCVS ? A git ?
 * Avoir un repository de travail en interne ministère avant de contribuer (au sein de la formation des agents)
 * Ne jamais utiliser un compte individuel pour publier un projet (qu'il soit personnel ou professionnel), mais créer un compte d'organisation (spécifique au projet ou rejoignant une organisation).
 * Bonne pratiques pour synchroniser des repositories entre eux (ex : github et gitlab)

3.3. Suivi de version
----------------------
 * Utilisation de semver: http://semver.org/

3.4. Fichiers obligatoires et headers
--------------------------------------
 * Avoir obligatoirement un fichier README, LICENSE, CONTRIBUTING et AUTHORS
 * Initiative intéressante de docker sur les rôles et les mainteners : https://github.com/docker/opensource/blob/master/MAINTAINERS#L34

 * Header de licence dans chaque fichier: https://forum.etalab.gouv.fr/t/faut-il-mettre-un-entete-dans-chaque-fichier-source/297


3.5. Porteur et gouvernance
-----------------------------

 * Besoin de créer un compte projet ou utiliser un compte d'Etat ou de structure
 * Créer une association ou transférer à une association
 * Gérer avec un contributor's agreeement : http://harmonyagreements.org/ 
 * Caler les droits des commiters
 * Pas d'autorisation d'une personne morale (prestataire) à avoir un role de commiter : préconnisation que ce soit uniquement des personnes physiques.

3.6. Enregistrement
-------------------

 * Avoir un annuaire de tous les comptes de tous les repositorys pour permettre de lister tous les projets. Ou et comment (reprendre ce que fait 18F ?)
 * Dans le cadre d'un projet initié par l'Etat, maintient de l'entrée dans l'annuaire, même si aucun agent n'a gardé un rôle de commiter
 * Dans le cadre d'un projet existant pour lequel un agent a un rôle de commiter, l'annuaire ne recense le projet que le temps ou l'agent est effectivement commiter (avec son compte professionnel).

3.7. Procédure de validation
----------------------------

#### 3.7.1. Validation technique

 * Peer Review

#### 3.7.2. Validation de sécurité

 * Validation par l'ANSSI ? (a posteriori?)
 - [ ] Pas de mot de passe dans le code
 - [ ] Pas d'adresses ou configuration d'environnement
 - [ ] Pas d'information personnelle

#### 3.7.3. Validation d'accessibilité

 * Outil pour vérifier l'accessibilité des solutions développées ?

#### 3.7.4. Validation juridique

 * Proposition de support par la DINSIC ?
 * Respect de conformité : proposer idéalement un suivi SPDX

OFFRE DE SERVICE DE LA DINSIC POUR PROPOSER DU SUPPORT sur la partie sécurité, accessibilité et juridique ?

ANNEXES
=======

A1. Contributor's agreement
---------------------------

Donner une référence à un accord de contribution.

A2. Liste des projets avec engagements d'Etat
---------------------------------------------

Ces projets sont officiellement supportés par l'Etat avec des ETP qui contribuent

A3. Annuaires des projets open-source d'Etat
--------------------------------------------

L'enregistrement des comptes github, adullact, framasoft, etc. se trouve ici

A4. Références juridiques
-------------------------

Code de la propriété intelectuelle notamment pour les agents publics

A5. Contrats de support existant
--------------------------------

Modalités de sollicitation

A6. Références
--------------

 * Clausier marché public: [http://www.economie.gouv.fr/apie/administrations-logiciels-libres-clausier-marches-publics](http://www.economie.gouv.fr/apie/administrations-logiciels-libres-clausier-marches-publics)

[CC-BY Logo]: https://licensebuttons.net/l/by/3.0/88x31.png
[CC-BY link]: https://creativecommons.org/licenses/by/3.0/
[Circulaire Ayrault]: http://circulaire.legifrance.gouv.fr/pdf/2012/09/cir_35837.pdf
